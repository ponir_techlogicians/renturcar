#!/bin/bash
#
#
# Reload a running Django project.
#
#

# Jump into the venv.
DIR=$(dirname ${0})

VENV_PATH=$DIR"/renturcar/venv/"

if [ ! -e $VENV_PATH ]
then
    echo "No venv, use local_deploy first."
    exit
fi

source $VENV_PATH/bin/activate

pip install -r $VENV_PATH/../requirements.txt

python $DIR/renturcar/manage.py makemigrations
python $DIR/renturcar/manage.py migrate

vagrant ssh -c "sudo touch /etc/uwsgi/vassals/renturcar.ini"

deactivate
