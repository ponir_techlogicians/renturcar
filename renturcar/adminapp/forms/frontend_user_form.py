from django import forms
from api.models import Person , MyUser
from datetime import  datetime

class FrontendUserForm(forms.ModelForm):
	username=forms.CharField(widget=forms.TextInput(attrs={'class':'form-control','required':False}))
	email=forms.EmailField(widget=forms.EmailInput(attrs={'class':'form-control','required': False}))
	firstname=forms.CharField(widget=forms.TextInput(attrs={'class':'form-control','required': False}))
	lastname=forms.CharField(widget=forms.TextInput(attrs={'class':'form-control','required': False}))
	password=forms.CharField(widget=forms.PasswordInput(attrs={'class':'form-control','required': False}))
	date_of_birth = forms.DateField(widget=forms.widgets.DateInput(format="%d/%m/%Y",attrs={'class':'form-control','required': False,'type':'date'}))
	def __init__(self, *args, **kwargs):
		super(FrontendUserForm,self).__init__(*args, **kwargs)

	class Meta:
		model=Person
		fields = ['username','firstname','lastname']


        def save(self,commit=True):
            email = self.cleaned_data['email']
            newUser = MyUser.objects.filter(email=email)
            if newUser.exists() and newUser[0].email != self.cleaned_data['email']:
                raise forms.ValidationError("please this email already exists")

            firstname = self.cleaned_data['firstname']
            lastname = self.cleaned_data['lastname']
            date_of_birth = self.cleaned_data['date_of_birth']
            password = self.cleaned_data['password']
            username = self.cleaned_data['username']

            user = MyUser()
            user.email = email
            user.set_password(password)
            user.is_admin=True
            user.save()

            p = Person()
            p.firstName = firstname
            p.lastName = lastname
            p.dateOfBirth = date_of_birth
            p.displayName = username
            p.user = user
            p.save()
            return p
