from django import forms
from api.models import Admin , MyUser
from datetime import  datetime
USER_CHOICES = (
    ('super_admin', "Super User"),
    ('staff', "Staff")
)

class UserForm(forms.ModelForm):
	username=forms.CharField(widget=forms.TextInput(attrs={'class':'form-control','required':False}))
	email=forms.EmailField(widget=forms.EmailInput(attrs={'class':'form-control','required': False}))
	firstname=forms.CharField(widget=forms.TextInput(attrs={'class':'form-control','required': False}))
	lastname=forms.CharField(widget=forms.TextInput(attrs={'class':'form-control','required': False}))
	role=forms.ChoiceField(choices = USER_CHOICES,widget=forms.Select(attrs={'class':'form-control','required': True}), label="User Type")
	password=forms.CharField(widget=forms.PasswordInput(attrs={'class':'form-control','required': False}))
	date_of_birth = forms.DateField(widget=forms.widgets.DateInput(format="%Y-%m-%d",attrs={'class':'form-control','required': False}))

	def __init__(self, *args, **kwargs):
		super(UserForm,self).__init__(*args, **kwargs)

	class Meta:
		model=Admin
		fields = ['username','firstname','lastname']
		

	def save(self,commit=True):
		email = self.cleaned_data['email']
		newUser = MyUser.objects.filter(email=email)
		if newUser.exists() and newUser[0].email != self.cleaned_data['email']:
			raise forms.ValidationError("please this email already exists")

		firstname = self.cleaned_data['firstname']
		lastname = self.cleaned_data['lastname']
		date_of_birth = self.cleaned_data['date_of_birth']
		role = self.cleaned_data['role']
		password = self.cleaned_data['password']
		username = self.cleaned_data['username']

		user = MyUser()
		user.email = email
		user.set_password(password)
		user.is_admin=True
		user.save()

		admin = Admin()
		admin.firstname = firstname
		admin.lastname = lastname
		admin.date_of_birth = date_of_birth
		admin.role= role
		admin.username = username
		admin.user = user
		admin.save()
		return admin



class UserEditForm(forms.ModelForm):
	username=forms.CharField(widget=forms.TextInput(attrs={'class':'form-control','required':False}))
	firstname=forms.CharField(widget=forms.TextInput(attrs={'class':'form-control','required': False}))
	lastname=forms.CharField(widget=forms.TextInput(attrs={'class':'form-control','required': False}))
	role=forms.ChoiceField(choices = USER_CHOICES,widget=forms.Select(attrs={'class':'form-control','required': True}), label="User Type")
	date_of_birth = forms.DateField(widget=forms.widgets.DateInput(format="%Y-%m-%d",attrs={'class':'form-control','required': False}))

	class Meta:
		model=Admin
		fields = ['username','firstname','lastname','role' ,'date_of_birth']



class UserProfileForm(forms.ModelForm):
	username=forms.CharField(widget=forms.TextInput(attrs={'class':'form-control','required':False}))
	firstname=forms.CharField(widget=forms.TextInput(attrs={'class':'form-control','required': False}))
	lastname=forms.CharField(widget=forms.TextInput(attrs={'class':'form-control','required': False}))
	date_of_birth = forms.DateField(widget=forms.widgets.DateInput(format="%Y-%m-%d",attrs={'class':'form-control','required': False}))

	class Meta:
		model=Admin
		fields = ['username','firstname','lastname','date_of_birth']


class UserChangePasswordForm(forms.Form):
	current_password=forms.CharField(widget=forms.PasswordInput(attrs={'class':'form-control','required': True}))
	new_password=forms.CharField(widget=forms.PasswordInput(attrs={'class':'form-control','required': True}))
	confirm_password=forms.CharField(widget=forms.PasswordInput(attrs={'class':'form-control','required': True}))

	def clean(self):
		if( self.cleaned_data.get('new_password') != self.cleaned_data.get('confirm_password')):
			raise forms.ValidationError("do not match the password")
		return self.cleaned_data









