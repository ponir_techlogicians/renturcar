from django.conf.urls import url
from views.login_view import LoginView
from .views.login_view import LogoutView
from .views.dashboard_view import DashboardView
from .views.admin_view import AdminCreate,AdminList,AdminDelete,AdminUpdate,AdminProfileUpdate,AdminPasswordChange
from django.contrib.auth.decorators import login_required
from .views.user_view import UserView,UserDetailView,UserApprove,UserDelete,UserCreateView
from .views.car_view import CarView,CarDetailView,CarApprove,CarDelete,CarCreateView
from .views.report_view import ReportCarView,ReportDailyUpdateView
urlpatterns = [

    # url(r'^admin/', admin.site.urls),
    url(r'^$',  LoginView.as_view()),
    url(r'^logout/$',  LogoutView.as_view(),name="logout"),
    url(r'^dashboard/$',login_required(DashboardView.as_view()),name="dashboard"),
    url(r'^add-admin/$',login_required(AdminCreate.as_view()),name="add-admin"),
    url(r'^list-admin/$',login_required(AdminList.as_view()),name="list-admin"),
    url(r'^delete-admin/(?P<id>[0-9]+)/$',login_required(AdminDelete.as_view()),name="delete-admin"),
    url(r'^update-admin/(?P<id>[0-9]+)/$',login_required(AdminUpdate.as_view()),name="update-admin"),
    url(r'^update-profile/$',login_required(AdminProfileUpdate.as_view()),name="update-profile"),
    url(r'^change-password/$',login_required(AdminPasswordChange.as_view()),name="change-password"),

    # user
    url(r'^user/(?P<status>[\w-]+)/$',login_required(UserView.as_view()),name="user"),
    url(r'^user/details/(?P<id>[0-9]+)/$',login_required(UserDetailView.as_view()),name="user-details"),
    url(r'^user-approve/$',login_required(UserApprove.as_view()),name="user-approve"),
    url(r'^delete-user/(?P<id>[0-9]+)/$', login_required(UserDelete.as_view()),name="delete-user"),
    url(r'^add-user/$',login_required(UserCreateView.as_view()),name="add-user"),
    #car
    url(r'^car/(?P<status>[\w-]+)/$',login_required(CarView.as_view()),name="car"),
    url(r'^car/details/(?P<id>[0-9]+)/$',login_required(CarDetailView.as_view()),name="car-details"),
    url(r'^car-approve/$',login_required(CarApprove.as_view()),name="car-approve"),
    url(r'^delete-car/(?P<id>[0-9]+)/$', login_required(CarDelete.as_view()),name="delete-car"),
    url(r'^add-car/$',login_required(CarCreateView.as_view()),name="add-car"),
    #report
    url(r'^report-car-view/$',login_required(ReportCarView.as_view()),name="report-car-view"),
    url(r'^report-daily-update/$',login_required(ReportDailyUpdateView.as_view()),name="report-daily-update"),
]

