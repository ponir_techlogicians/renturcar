from django.views.generic.edit import CreateView,UpdateView,DeleteView
from adminapp.forms.user_forms import UserForm,UserEditForm,UserProfileForm,UserChangePasswordForm
from api.models import Admin , MyUser
from django.core.urlresolvers import reverse
from django.views.generic.list import ListView
from django.views.generic import TemplateView
from django.http import HttpResponseRedirect
from django.contrib.auth import update_session_auth_hash
from django.contrib import messages
from django.shortcuts import redirect
class AdminCreate(CreateView):
    template_name = "admin/form.html"
    form_class=UserForm

    def get_context_data(self,**kwargs):
        context=super(AdminCreate,self).get_context_data(**kwargs)
        context['title']="Create Team member"
        return context

    def get_success_url(self):
        return reverse('list-admin')


class AdminList(ListView):
    template_name = "admin/list.html"
    model = Admin

    def get_context_data(self, **kwargs):
        context = super(AdminList, self).get_context_data(**kwargs)
        return context


class AdminDelete(DeleteView):
    template_name = "admin/delete.html"
    model = Admin

    def get_context_data(self,**kwargs):
        context=super(AdminDelete,self).get_context_data(**kwargs)
        return context

    def get_object(self):
        user = Admin.objects.get(id=self.kwargs['id'])
        if self.request.user.id == user.user_id:
            user.same_user = True
        else:
            user.same_user = False
        return user

    def delete(self,request, *args, **kwargs):
        self.object = self.get_object()
        if self.request.user.id == self.object.user_id:
            raise Exception('you can not delete yourself')
        self.object.delete()
        MyUser.objects.get(id=self.object.user_id).delete()
        return HttpResponseRedirect(reverse('list-admin'))


    def get_success_url(self):
        return reverse('list-admin')


class AdminUpdate(UpdateView):

    template_name = "admin/update.html"
    model = Admin
    form_class=UserEditForm

    def get_object(self):
        return Admin.objects.get(id=self.kwargs['id'])

    def get_success_url(self):
        return reverse('list-admin')


class AdminProfileUpdate(UpdateView):

    template_name = "admin/profile.html"
    model = Admin
    form_class=UserProfileForm


    def get_object(self):
        object = Admin.objects.filter(user_id=self.request.user.id)
        if object.exists():
            self.object = object.last()
        else:
            self.object = None
        return self.object

    def get_success_url(self):
        return reverse('dashboard')

    def post(self,request,*args,**kwargs):
        form_class = self.get_form_class()
        form = self.get_form(form_class)
        if form.is_valid():
            object = self.get_object()
            if object:
                return super(AdminProfileUpdate, self).post(request, *args, **kwargs)
            else:
                obj = Admin()
                obj.username =form.cleaned_data['username']
                obj.firstname =form.cleaned_data['firstname']
                obj.lastname =form.cleaned_data['lastname']
                obj.role = 'super_admin'
                obj.date_of_birth = form.cleaned_data['date_of_birth']
                obj.user_id = self.request.user.id
                obj.save()

        return HttpResponseRedirect(reverse('dashboard'))

class AdminPasswordChange(TemplateView):
	template_name='admin/change_password.html'
	form_class=UserChangePasswordForm

	def get_context_data(self,**kwargs):

		context = super(AdminPasswordChange, self).get_context_data(**kwargs)
		context['title']='Change Password'
		context['form'] = UserChangePasswordForm(self.request.POST or None);
		return context


	def post(self,request,commit=True):
		form = UserChangePasswordForm(self.request.POST or None);

		if form.is_valid():

			current_password = form.cleaned_data['current_password']
			new_password = form.cleaned_data['new_password']

			isPassCorrect = self.request.user.check_password(current_password)
			if isPassCorrect:
				user = MyUser.objects.get(id=self.request.user.id)
				user.set_password(new_password)
				user.save()
				update_session_auth_hash(request, user)
				messages.success(request, 'Your password was updated successfully!')
				# User.objects.filter(password=password,id=self.request.user.id).update(password=newPass)
				return redirect('change-password')
			else:
				messages.warning(request, 'Incorrect Current Password')
				return redirect('change-password')
		else:
			messages.error(request, 'do not match the password',extra_tags='alert-danger')
			return redirect('change-password')
		# return render_to_response( self.template_name, {'form': form})

		def get_success_url(self):
			return reverse('list-admin')