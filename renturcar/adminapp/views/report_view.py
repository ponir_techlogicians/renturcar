from django.views.generic import TemplateView
from django.views.generic.edit import CreateView,DeleteView
from django.http import JsonResponse
import datetime
import json
from django.contrib.auth import (
    authenticate,
    get_user_model,
    login,
    logout
    )
from django.core.urlresolvers import reverse
from django.http import HttpResponse
from api.models import Person , Vehicle , PersonDocument,VehicleDocument,Booking
from datetime import datetime, timedelta, time

class ReportCarView(TemplateView):
    template_name='report/car.html'
    def get_context_data(self,**kwargs):
        context = super(ReportCarView, self).get_context_data(**kwargs)
        vehicles = Vehicle.objects.filter(number_of_views__gt=0)
        context['vehicles']=vehicles
        return context

class ReportDailyUpdateView(TemplateView):
    template_name='report/daily_update.html'
    def get_context_data(self,**kwargs):
        today = datetime.now().date()
        tomorrow = today + timedelta(1)
        today_start = datetime.combine(today, time())
        today_end = datetime.combine(tomorrow, time())

        context = super(ReportDailyUpdateView, self).get_context_data(**kwargs)
        vehicles = Vehicle.objects.filter(number_of_views__gt=0)
        context['vehicles']=vehicles

        number_of_registeredUser_today = Person.objects.filter(created_at__gte=today_start,created_at__lt=today_end).count()
        number_of_registeredVehicle_today = Vehicle.objects.filter(created_at__gte=today_start,created_at__lt=today_end).count()
        number_of_bookingConfirmed_today = Booking.objects.filter(confirmedByOwner=True,created_at__gte=today_start,created_at__lt=today_end).count()
        context['registered_user']=number_of_registeredUser_today
        context['registered_car']=number_of_registeredVehicle_today
        context['booking_confirmed']=number_of_bookingConfirmed_today
        return context
