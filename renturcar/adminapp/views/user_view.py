
from django.views.generic import TemplateView
from django.views.generic.edit import CreateView,DeleteView
from django.http import JsonResponse
import datetime
import json
from django.contrib.auth import (
    authenticate,
    get_user_model,
    login,
    logout
    )
from django.core.urlresolvers import reverse
from django.http import HttpResponse
from api.models import Person , Vehicle , PersonDocument,MyUser
from adminapp.forms.frontend_user_form import FrontendUserForm

class UserCreateView(CreateView):
    template_name = "user/form.html"
    form_class=FrontendUserForm

    def get_context_data(self,**kwargs):
        context=super(UserCreateView,self).get_context_data(**kwargs)
        context['title']="Create User"
        return context

    def get_success_url(self):
        return reverse('user',kwargs={'status':'all'})

class UserView(TemplateView):

    template_name='user/user.html'
    def get_context_data(self,**kwargs):
        status = kwargs.get('status')
        context = super(UserView, self).get_context_data(**kwargs)

        persons = Person.objects.all()
        context['total_users'] = len(persons)
        if status == 'pending':
            context['title'] = 'Pending User'
            context['type']='pending'
            pending_users = []
            for person in persons:
                if not person.is_verified():
                    pending_users.append(person)
            context['users']=pending_users
        elif status == 'approved':
            context['title'] = 'Approved User'
            approved_users = []
            for person in persons:
                if person.is_verified():
                    approved_users.append(person)
            context['users']=approved_users
            context['type']='approved'
        else:
            context['title'] = 'All User'
            context['users']=persons
            context['type']='all'
        return context

class UserDetailView(TemplateView):
    template_name = "user/details.html"

    def get_context_data(self, **kwargs):
        user_id = kwargs.get('id')
        context = super(UserDetailView, self).get_context_data(**kwargs)
        context['person']= Person.objects.get(id=user_id)
        context['documents']= PersonDocument.objects.filter(person_id=user_id)
        return context

class UserApprove(CreateView):
    def post(self, request, *args, **kwargs):
            user_id = request.POST.get('user_id')
            person = Person.objects.get(id=user_id)
            person.approved = True
            person.save()

            documents = PersonDocument.objects.filter(person_id=user_id)
            for document in documents:
                document.verification.verified = True
                document.verification.save()

            response_data={}
            response_data['status']=True
            return HttpResponse(json.dumps(response_data), content_type="application/json")

class UserDelete(DeleteView):
    template_name='user/delete_confirm.html'
    def get_object(self,**kwargs):
        person = Person.objects.get(id=self.kwargs['id'])
        return MyUser.objects.get(id=person.user.id)

    def get_success_url(self):
        return reverse('user',kwargs={'status':'all'})
