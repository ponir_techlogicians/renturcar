from django.views.generic import TemplateView
from django.views.generic.edit import CreateView,DeleteView
from django.http import JsonResponse
import datetime
import json
from django.contrib.auth import (
    authenticate,
    get_user_model,
    login,
    logout
    )
from django.core.urlresolvers import reverse
from django.http import HttpResponse
from api.models import Person , Vehicle , PersonDocument,VehicleDocument,BodyType,CostPerDay,Address,VehiclePhoto
from django.shortcuts import render,redirect

class CarCreateView(TemplateView):
    template_name = 'car/form.html'

    def address_get_or_create(self,lineOne,city,country,post_code):
        address_obj = None
        address = Address.objects.filter(lineOne=lineOne,lineTwo=city,country=country,postCode=post_code)
        if address.exists():
            address_obj = address.first()
        else:
            address_obj = Address()
            address_obj.lineOne=lineOne
            address_obj.lineTwo=city
            address_obj.country=country
            address_obj.postCode=post_code
            address_obj.save()
        return address_obj

    def get(self,request,*args,**kwargs):
        context={}
        context['title']='Add car'
        context['bodytypes']=BodyType.objects.all()
        context['persons']= Person.objects.all()
        return render(request,self.template_name,context)

    def post(self,request,*args,**kwargs):
        person = request.POST.get('user')
        body_type = request.POST.get('bodytype')
        location = request.POST.get('location')
        price = request.POST.get('price')
        make = request.POST.get('make')
        model = request.POST.get('model')
        licenceplate = request.POST.get('licence')
        year = request.POST.get('year')
        kilo = request.POST.get('kilometer')
        summary = request.POST.get('summary')
        color = request.POST.get('color')
        photo=request.FILES['photo']
        insurance_3fs=request.FILES['finsurance']
        private_insurane = request.FILES['pinsurance']

        v = Vehicle()
        v.make=make
        v.model=model
        v.year=year
        v.licensePlate=licenceplate
        v.colour=color
        v.comments=summary
        v.bodyType_id=body_type
        v.owner_id=person
        c = CostPerDay()
        c.rentalCost = price
        c.commissionCost = 0.0
        c.save()
        v.defaultCost = c

        lineOne=[]
        lineTwo=''
        country=''
        postal_code=''
        if 'street_number' in request.POST:
            street_number = request.POST['street_number']
            lineOne.append(street_number)

        if 'route' in request.POST:
            route = request.POST['route']
            lineOne.append(route)

        line1 = ''
        line1 = ' '.join(lineOne)
        if 'locality' in request.POST:
            lineTwo = request.POST['locality']
        if 'country' in request.POST:
            country = request.POST['country']
        if 'postal_code' in request.POST:
            postal_code = request.POST['postal_code']

        v.address = self.address_get_or_create(line1,lineTwo,country,postal_code)
        v.rules = ''
        v.minRentDays = 0
        v.maxRentDays = 0
        v.advanceNotice = 0
        v.preparationTime = 0
        v.save()
        vp = VehiclePhoto()
        vp.photo = photo
        vp.vehicle = v
        vp.save()

        vd= VehicleDocument(vehicle=v,document=private_insurane)
        vd.save()

        vi= VehicleDocument(vehicle=v,document=insurance_3fs)
        vi.save()

        return redirect('/admin/car/details/'+str(v.id)+'/')

    def get_success_url(self):
        return reverse('car',kwargs={'status':'all'})

class CarView(TemplateView):

    template_name='car/car.html'
    def get_context_data(self,**kwargs):
        status = kwargs.get('status')
        context = super(CarView, self).get_context_data(**kwargs)
        vehicles = Vehicle.objects.all()
        context['total_cars'] = len(vehicles)
        if status == 'pending':
            pending_vehicles = []
            context['title']= 'Pending cars'
            context['type']='pending'
            for vehicle in vehicles:
                if not vehicle.is_verified():
                    pending_vehicles.append(vehicle)
            context['vehicles']=pending_vehicles

        elif status == 'approved':
            context['title'] = 'Approved Cars'
            approved_cars = []
            for vehicle in vehicles:
                if vehicle.is_verified():
                    approved_cars.append(vehicle)
            context['vehicles']=approved_cars
            context['type']='approved'
        else:
            context['title'] = 'All Cars'
            context['vehicles']=vehicles
            context['type']='all'
        return context

class CarDetailView(TemplateView):
    template_name = "car/details.html"

    def get_context_data(self, **kwargs):
        vehicle_id = kwargs.get('id')
        context = super(CarDetailView, self).get_context_data(**kwargs)
        context['vehicle']= Vehicle.objects.get(id=vehicle_id)
        context['documents']= VehicleDocument.objects.filter(vehicle_id=vehicle_id)
        return context


class CarApprove(CreateView):
    def post(self, request, *args, **kwargs):
            car_id = request.POST.get('car_id')

            documents = VehicleDocument.objects.filter(vehicle_id=car_id)
            for document in documents:
                document.verification.verified = True
                document.verification.save()

            response_data={}
            response_data['status']=True
            return HttpResponse(json.dumps(response_data), content_type="application/json")

class CarDelete(DeleteView):
    template_name='car/delete_confirm.html'
    def get_object(self,**kwargs):
        return Vehicle.objects.get(id=self.kwargs['id'])

    def get_success_url(self):
        return reverse('car',kwargs={'status':'all'})