
from django.views.generic import TemplateView

import datetime

from django.contrib.auth import (
	authenticate,
	get_user_model,
	login,
	logout
	)
from api.models import Person , Vehicle

class DashboardView(TemplateView):

	template_name='dashboard/dashboard.html'
	
	def get_context_data(self,**kwargs):
		context = super(DashboardView, self).get_context_data(**kwargs)
		pending_users = []
		persons = Person.objects.all()
		context['total_users'] = len(persons)
		for person in persons:
			if not person.is_verified():
				pending_users.append(person)

		context['pending_users']=pending_users[:10]
		context['pending_users_count']=len(pending_users)

		pending_vehicles = []
		vehicles = Vehicle.objects.all()
		context['total_vehicles']=len(vehicles)
		for vehicle in vehicles:
			if not vehicle.is_verified():
				pending_vehicles.append(vehicle)

		context['pending_vehicles']=pending_vehicles[:10]
		context['pending_vehicles_count']=len(pending_vehicles)
		context['car_approved'] = len(vehicles)-len(pending_vehicles)
		return context