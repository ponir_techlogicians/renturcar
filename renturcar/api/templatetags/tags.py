import pendulum
from django.utils.html import format_html
from api.models import Vehicle

from django.template import Library
register = Library()


@register.filter(name='roundedrange')
def filter_rounded_range(start, end):
    return filter_range(start, filter_round(end))


@register.filter(name='round')
def filter_round(num):
    return int(num)


@register.filter(name="negate")
def filter_negate(num):
    return num * -1


@register.filter(name='range')
def filter_range(start, end):
    return range(start, end)


@register.filter(name="dateformat")
def filter_dateformat(date):
    dt = pendulum.date.instance(date)
    return dt.to_date_string()


@register.filter
def get_item(dictionary, key):
    return dictionary.get(key)

@register.filter
def get_prop(dictionary, key):
    return dictionary.key

@register.filter
def rating_stars(vehicle):
    if type(vehicle) is not Vehicle:
        return format_html("")

    else:
        if vehicle.rating():
            string = "<div class=\"stars\">"
            rating = int(vehicle.rating())

            for i in range(0, rating):
                string += "<span class=\"glyphicon glyphicon-star\" aria-hidden\"true\"></span>"

            for i in range(rating, 5):
                string += "<span class=\"glyphicon glyphicon-star-empty\" aria-hidden=\"true\"></span>"

            string += "</div>"
            return format_html(string)

        else:
            return format_html("<div class=\"stars\">No rating</div>")


@register.filter
def modulo(a, b):
    return a % b
