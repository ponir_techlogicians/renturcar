# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='MyUser',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('password', models.CharField(max_length=128, verbose_name='password')),
                ('last_login', models.DateTimeField(null=True, verbose_name='last login', blank=True)),
                ('email', models.EmailField(unique=True, max_length=255, verbose_name=b'email address')),
                ('is_active', models.BooleanField(default=True)),
                ('is_admin', models.BooleanField(default=False)),
            ],
            options={
                'abstract': False,
            },
        ),
        migrations.CreateModel(
            name='Address',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('lineOne', models.CharField(max_length=200)),
                ('lineTwo', models.CharField(max_length=200)),
                ('country', models.CharField(max_length=50)),
                ('postCode', models.CharField(max_length=30)),
            ],
        ),
        migrations.CreateModel(
            name='Person',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('dateOfBirth', models.DateTimeField()),
                ('phone', models.CharField(max_length=20)),
                ('address', models.ForeignKey(to='api.Address')),
                ('user', models.OneToOneField(to=settings.AUTH_USER_MODEL)),
            ],
        ),
        migrations.CreateModel(
            name='Vehicle',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('make', models.CharField(max_length=100)),
                ('model', models.CharField(max_length=100)),
                ('year', models.IntegerField()),
                ('colour', models.CharField(max_length=50)),
                ('licensePlate', models.CharField(max_length=50)),
                ('vin', models.CharField(max_length=50)),
                ('comments', models.CharField(max_length=500)),
                ('rules', models.CharField(max_length=500)),
                ('minRentDays', models.IntegerField()),
                ('maxRentDays', models.IntegerField()),
                ('advanceNotice', models.IntegerField()),
                ('preparationTime', models.IntegerField()),
                ('address', models.ForeignKey(to='api.Address')),
                ('owner', models.ForeignKey(to='api.Person')),
            ],
        ),
        migrations.CreateModel(
            name='VehiclePhoto',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('vehicle', models.ForeignKey(to='api.Vehicle')),
            ],
        ),
    ]
