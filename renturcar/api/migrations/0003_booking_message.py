# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('api', '0002_auto_20161028_0818'),
    ]

    operations = [
        migrations.CreateModel(
            name='Booking',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('startTime', models.DateTimeField()),
                ('endTime', models.DateTimeField()),
                ('confirmedByOwner', models.BooleanField(default=False)),
                ('cost', models.ForeignKey(to='api.CostPerDay')),
                ('renter', models.ForeignKey(to='api.Person')),
                ('vehicle', models.ForeignKey(to='api.Vehicle')),
            ],
        ),
        migrations.CreateModel(
            name='Message',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('timestamp', models.DateTimeField()),
                ('readTimestamp', models.DateTimeField()),
                ('message', models.CharField(max_length=500)),
                ('receiver', models.ForeignKey(related_name='+', to='api.Person')),
                ('sender', models.ForeignKey(related_name='+', to='api.Person')),
            ],
        ),
    ]
