# -*- coding: utf-8 -*-
# Generated by Django 1.10.4 on 2018-06-07 06:40
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('api', '0023_auto_20171115_1039'),
    ]

    operations = [
        migrations.CreateModel(
            name='PersonDocument',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('document', models.FileField(upload_to=b'')),
            ],
        ),
        migrations.AddField(
            model_name='person',
            name='gender',
            field=models.CharField(blank=True, max_length=100),
        ),
        migrations.AlterField(
            model_name='verification',
            name='verifying',
            field=models.CharField(choices=[(b'EMAIL', b'Email'), (b'DATEOFBIRTH', b'Date of Birth'), (b'ADDRESS', b'Address'), (b'PHONE', b'Phone'), (b'NAME', b'Name'), (b'VEHICLEDOCUMENTS', b'Vehicle Documents'), (b'PERSONDOCUMENTS', b'Person Documents')], max_length=50),
        ),
        migrations.AddField(
            model_name='persondocument',
            name='person',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='api.Person'),
        ),
        migrations.AddField(
            model_name='persondocument',
            name='verification',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, related_name='+', to='api.Verification'),
        ),
    ]
