# -*- coding: utf-8 -*-
# Generated by Django 1.10.4 on 2017-05-19 05:11
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('api', '0008_auto_20170505_1621'),
    ]

    operations = [
        migrations.AddField(
            model_name='vehiclephoto',
            name='photo',
            field=models.ImageField(default='', upload_to=b''),
            preserve_default=False,
        ),
    ]
