# -*- coding: utf-8 -*-
# Generated by Django 1.10.4 on 2017-06-20 04:59
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('api', '0019_auto_20170620_1154'),
    ]

    operations = [
        migrations.AlterField(
            model_name='verification',
            name='verifying',
            field=models.CharField(choices=[(b'EMAIL', b'Email'), (b'DATEOFBIRTH', b'Date of Birth'), (b'ADDRESS', b'Address'), (b'PHONE', b'Phone'), (b'NAME', b'Name')], max_length=50),
        ),
    ]
