# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('api', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='Availability',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('startTime', models.DateTimeField()),
                ('endTime', models.DateTimeField()),
            ],
        ),
        migrations.CreateModel(
            name='CostPerDay',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('rentalCost', models.FloatField()),
                ('commissionCost', models.FloatField()),
                ('currency', models.CharField(max_length=50)),
            ],
        ),
        migrations.CreateModel(
            name='Review',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('comment', models.CharField(max_length=500)),
                ('rating', models.IntegerField()),
                ('person', models.ForeignKey(to='api.Person')),
                ('vehicle', models.ForeignKey(to='api.Vehicle')),
            ],
        ),
        migrations.AddField(
            model_name='availability',
            name='cost',
            field=models.ForeignKey(blank=True, to='api.CostPerDay', null=True),
        ),
        migrations.AddField(
            model_name='availability',
            name='vehicle',
            field=models.ForeignKey(to='api.Vehicle'),
        ),
        migrations.AddField(
            model_name='vehicle',
            name='defaultCost',
            field=models.ForeignKey(default=1, to='api.CostPerDay'),
            preserve_default=False,
        ),
    ]
