# -*- coding: utf-8 -*-
# Generated by Django 1.10.4 on 2017-06-20 02:26
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('api', '0014_auto_20170618_1435'),
    ]

    operations = [
        migrations.AddField(
            model_name='person',
            name='displayName',
            field=models.CharField(blank=True, max_length=100),
        ),
    ]
