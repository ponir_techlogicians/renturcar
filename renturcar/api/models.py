from django.db import models
from django.contrib import admin
from django.contrib.auth.models import User
from django.db import models
from django.contrib.auth.models import (
    BaseUserManager, AbstractBaseUser
)
from django.db.models import Q
from django.template.defaulttags import register
from django.db.models import Avg

from datetime import datetime
import hashlib
import pendulum
import requests
import urllib2
import json
from datetime import timedelta
import smtplib
from django.core.mail import send_mail
from django.conf import settings
from django.db.models import Q
from django.utils import timezone
@register.filter
def get_item(dictionary, key):
    return dictionary.get(key)


class MyUserManager(BaseUserManager):
    def create_user(self, email, password=None):
        """
        Creates and saves a User with the given email
        and password.
        """
        if not email:
            raise ValueError('Users must have an email address')

        user = self.model(
            email=self.normalize_email(email),
        )

        user.set_password(password)
        user.save(using=self._db)
        return user

    def create_superuser(self, email, password):
        """
        Creates and saves a superuser with the given email
        password.
        """
        user = self.create_user(
            email,
            password=password,
        )
        user.is_admin = True
        user.save(using=self._db)
        return user


class MyUser(AbstractBaseUser):
    email = models.EmailField(
        verbose_name='email address',
        max_length=255,
        unique=True,
    )

    # Used to store a new email while verification of the address is pending.
    newEmail = models.EmailField(max_length=255, blank=True, null=True)

    is_active = models.BooleanField(default=True)
    is_admin = models.BooleanField(default=False)

    __original_newEmail = None

    objects = MyUserManager()

    USERNAME_FIELD = 'email'

    def __init__(self, *args, **kwargs):
        super(MyUser, self).__init__(*args, **kwargs)
        self.__original_newEmail = self.newEmail

    def save(self, *args, **kwargs):
        super(MyUser, self).save(*args, **kwargs)

        if self.newEmail != self.__original_newEmail:
            v = self.person.default_verification_object()
            v.verifying = Verification.VERIFYING_CHOICE_EMAIL
            v.save()
            self.person.emailVerification = v
            self.person.save()

        super(MyUser, self).save(*args, **kwargs)

        self.__original_newEmail = self.newEmail

    def get_full_name(self):
        # The user is identified by their email address
        return self.email

    def get_short_name(self):
        # The user is identified by their email address
        return self.email

    def __str__(self):              # __unicode__ on Python 2
        return self.email

    def has_perm(self, perm, obj=None):
        "Does the user have a specific permission?"
        # Simplest possible answer: Yes, always
        return True

    def has_module_perms(self, app_label):
        "Does the user have permissions to view the app `app_label`?"
        # Simplest possible answer: Yes, always
        return True

    @property
    def is_staff(self):
        "Is the user a member of staff?"
        # Simplest possible answer: All admins are staff
        return self.is_admin


class Address(models.Model):
    lineOne = models.CharField(max_length=200)
    lineTwo = models.CharField(max_length=200)
    country = models.CharField(max_length=50)
    postCode = models.CharField(max_length=30)

    def __str__(self):
        return self.lineTwo

admin.site.register(Address)

# new model admin Details

class Admin(models.Model):
    user = models.OneToOneField(MyUser)
    firstname = models.CharField(max_length=100, blank=True)
    lastname = models.CharField(max_length=100, blank=True)
    username = models.CharField(max_length=100, blank=True)
    date_of_birth = models.DateField()
    role = models.CharField(max_length=100)

class Person(models.Model):
    user = models.OneToOneField(MyUser)
    dateOfBirth = models.DateField()  # Age Validation
    address = models.ForeignKey(Address, null=True, blank=True)
    phone = models.CharField(max_length=20)
    profilePicture = models.ImageField(blank=True, null=True)
    firstName = models.CharField(max_length=100, blank=True)
    lastName = models.CharField(max_length=100, blank=True)
    displayName = models.CharField(max_length=100, blank=True)
    gender = models.CharField(max_length=100, blank=True)
    approved = models.BooleanField(default=False)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    __original_dateOfBirth = None
    __original_address = None
    __original_phone = None
    __original_firstName = None
    __original_lastName = None
    __original_email = None

    emailVerification = models.ForeignKey('Verification', null=True, blank=True, related_name="+")
    dateOfBirthVerification = models.ForeignKey('Verification', null=True, blank=True, related_name="+")
    addressVerification = models.ForeignKey('Verification', null=True, blank=True, related_name="+")
    phoneNumberVerification = models.ForeignKey('Verification', null=True, blank=True, related_name="+")
    firstNameVerification = models.ForeignKey('Verification', null=True, blank=True, related_name="+")
    lastNameVerification = models.ForeignKey('Verification', null=True, blank=True, related_name="+")

    def __unicode__(self):
        return self.user.email

    def __init__(self, *args, **kwargs):
        super(Person, self).__init__(*args, **kwargs)
        self.__original_dateOfBirth = self.dateOfBirth
        self.__original_address = self.address
        self.__original_phone = self.phone
        self.__original_firstName = self.firstName
        self.__original_lastName = self.lastName
        # self.__original_email = self.user.email

    def default_verification_object(self):
        v = Verification()
        v.code = v.generate_code(self)
        v.expires = pendulum.now('Australia/West').add(days=7)
        v.person = self
        return v

    def save(self, *args, **kwargs):

        if self.displayName == "" and self.firstName != "":
            self.displayName = self.firstName

        super(Person, self).save(*args, **kwargs)

        if self.dateOfBirth != self.__original_dateOfBirth:
            v = self.default_verification_object()
            v.verifying = Verification.VERIFYING_CHOICE_DATEOFBIRTH
            v.save()
            self.dateOfBirthVerification = v

        if self.address != self.__original_address:
            v = self.default_verification_object()
            v.verifying = Verification.VERIFYING_CHOICE_ADDRESS
            v.save()
            self.addressVerification = v

        if self.phone != self.__original_phone:
            v = self.default_verification_object()
            v.verifying = Verification.VERIFYING_CHOICE_PHONE
            v.save()
            self.phoneNumberVerification = v

        if self.firstName != self.__original_firstName:
            v = self.default_verification_object()
            v.verifying = Verification.VERIFYING_CHOICE_NAME
            v.save()
            self.firstNameVerification = v

        if self.lastName != self.__original_lastName:
            v = self.default_verification_object()
            v.verifying = Verification.VERIFYING_CHOICE_NAME
            v.save()
            self.lastNameVerification = v

        if self.user.email != self.__original_email:
            if not self.is_email_verified():
                v = self.default_verification_object()
                v.verifying = Verification.VERIFYING_CHOICE_EMAIL
                v.save()
                self.emailVerification = v

        super(Person, self).save(*args, **kwargs)

        self.__original_dateOfBirth = self.dateOfBirth
        self.__original_address = self.address
        self.__original_phone = self.phone
        self.__original_firstName = self.firstName
        self.__original_lastName = self.lastName

    def can_rent(self):
        if not self.approved:
            return False
        if self.emailVerification and not self.emailVerification.verified:
            return False

        if self.dateOfBirthVerification and not self.dateOfBirthVerification.verified:
            return False

        # if self.addressVerification and not self.addressVerification.verified:
        #     return False

        # if self.phoneNumberVerification and not self.phoneNumberVerification.verified:
        #     return False

        if self.firstNameVerification and not self.firstNameVerification.verified:
            return False
        else:
            valid = True
            driving_licences = PersonDocument.objects.filter(person=self)
            if driving_licences.exists():
                for licence in driving_licences:
                    if not licence.verification.verified:
                        valid=False
                        break
            else:
                valid=False

            return valid

    def is_licence_uploaded(self):
        valid = False
        driving_licences = PersonDocument.objects.filter(person=self)
        if driving_licences.exists():
            valid=True
        return valid

    def is_profile_picture_uploaded(self):
        valid =True
        if not self.profilePicture:
            valid = False
        return valid

    def is_phone_number_added(self):
        valid =True
        if not self.phone:
            valid = False
        return valid

    def is_address_added(self):
        valid =True
        if not self.address:
            valid = False
        return valid

    def is_verified(self):
        admin_approved = False
        if self.approved:
            admin_approved = True
        else:
            admin_approved = False

        # valid = True
        # driving_licences = PersonDocument.objects.filter(person=self)
        # if driving_licences.exists():
        #     for licence in driving_licences:
        #         if not licence.verification.verified:
        #             valid=False
        #             break
        # else:
        #     valid=False

        # if admin_approved and valid:
        if admin_approved :
            return True
        else:
            return False

    def is_email_verified(self):
        if self.emailVerification and self.emailVerification.verified:
            return True
        else:
            return False

admin.site.register(Person)

class PersonDocument(models.Model):
    person = models.ForeignKey(Person)
    document = models.FileField()
    licence_number = models.CharField(max_length=100,blank=True)
    licence_state = models.CharField(max_length=100,blank=True)
    country = models.CharField(max_length=100,blank=True)
    expiry = models.DateField(blank=True,null=True)
    verification = models.ForeignKey('Verification', null=True, blank=True, related_name="+")


    def save(self, *args, **kwargs):

        if not self.verification:
            v = Verification()
            v.code = v.generate_code(self)
            v.expires = pendulum.now('Australia/West').add(days=30)
            v.verifying = Verification.VERIFYING_CHOICE_PERSONDOCUMENTS
            v.person = self.person
            v.save()
            self.verification = v

        super(PersonDocument, self).save(*args, **kwargs)

admin.site.register(PersonDocument)


class CostPerDay(models.Model):
    rentalCost = models.FloatField()
    commissionCost = models.FloatField()
    currency = models.CharField(max_length=50)

    def totalCost(self):
        return self.rentalCost + self.commissionCost

admin.site.register(CostPerDay)


class BodyType(models.Model):
    name = models.CharField(max_length=100)
    image = models.ImageField(blank=True, null=True)

admin.site.register(BodyType)


class Vehicle(models.Model):
    make = models.CharField(max_length=100)  # Predefined list?
    model = models.CharField(max_length=100)  # Predefined list?
    year = models.IntegerField()  # Limits?
    colour = models.CharField(max_length=50)  # Predefined list?
    licensePlate = models.CharField(max_length=50)
    vin = models.CharField(max_length=50)  # Validation
    address = models.ForeignKey(Address)
    owner = models.ForeignKey(Person)
    comments = models.CharField(max_length=500)
    rules = models.CharField(max_length=500)
    minRentDays = models.IntegerField()
    maxRentDays = models.IntegerField()
    advanceNotice = models.IntegerField()
    preparationTime = models.IntegerField()
    defaultCost = models.ForeignKey(CostPerDay)
    bodyType = models.ForeignKey(BodyType)
    hasBond = models.BooleanField(default=True)
    bondAmmount = models.FloatField(default=50.0)
    number_of_views = models.IntegerField(default=0)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    def showInSearch(self):
        # For a vehicle to be rentable, it must have at least one document that
        # has been approved.
        documents = VehicleDocument.objects.filter(vehicle=self)
        if documents.count() < 1:
            return False

        verified = True
        for doc in documents:
            if not doc.verification.verified:
                verified = False
                break

        return verified

    def is_verified(self):
        verified = True
        documents = VehicleDocument.objects.filter(vehicle=self)
        if not documents.exists():
            verified= False
        else:
            verified = True
            for doc in documents:
                if not doc.verification.verified:
                    verified = False
                    break

        return verified


    def rating(self):
        rating = Review.objects.filter(vehicle=self).aggregate(rating=Avg('rating'))['rating']
        return rating

    def totalCost(self):
        if self.hasBond:
            return self.defaultCost.totalCost() + self.bondAmmount

        else:
            return self.defaultCost.totalCost()

    def vehicleAvailableOnDate(self, date):
        # Vehicles can't be available if they can't be rented.
        if not self.showInSearch():
            return False

        # Get the bookings for the vehicle and check that the date isn't covered by one.
        bookings = Booking.objects.filter(vehicle=self).filter(Q(confirmedByOwner=True))
        available = True
        for b in bookings:
            # add preptime to start and end.
            if date > pendulum.instance(b.startTime).subtract(days=self.preparationTime) and date < pendulum.instance(b.endTime).add(days=self.preparationTime):
                available = False

        return available

    def vehicle_thumb_photo(self):
        thumb_photo = VehiclePhoto.objects.filter(vehicle=self).first()
        return thumb_photo

    def increase_number_of_view(self):
        self.number_of_views=self.number_of_views+1
        self.save()


admin.site.register(Vehicle)


class VehiclePropertyName(models.Model):
    name = models.CharField(max_length=100)


class VehiclePropertyOption(models.Model):
    propertyName = models.ForeignKey(VehiclePropertyName)
    option = models.CharField(max_length=100)


class VehicleProperty(models.Model):
    vehicle = models.ForeignKey(Vehicle)
    propertyName = models.ForeignKey(VehiclePropertyName)
    value = models.CharField(max_length=200)
    option = models.BooleanField(default=False)


admin.site.register(VehicleProperty)
admin.site.register(VehiclePropertyName)
admin.site.register(VehiclePropertyOption)


class VehiclePhoto(models.Model):
    vehicle = models.ForeignKey(Vehicle)
    photo = models.ImageField()

admin.site.register(VehiclePhoto)

class VehicleDocument(models.Model):
    vehicle = models.ForeignKey(Vehicle)
    document = models.FileField()
    verification = models.ForeignKey('Verification', null=True, blank=True, related_name="+")


    def save(self, *args, **kwargs):

        if not self.verification:
            v = Verification()
            v.code = v.generate_code(self)
            v.expires = pendulum.now('Australia/West').add(days=30)
            v.verifying = Verification.VERIFYING_CHOICE_VEHICLEDOCUMENTS
            v.person = self.vehicle.owner
            v.save()
            self.verification = v

        super(VehicleDocument, self).save(*args, **kwargs)

admin.site.register(VehicleDocument)


class Availability(models.Model):
    startTime = models.DateTimeField()
    endTime = models.DateTimeField()
    vehicle = models.ForeignKey(Vehicle)
    cost = models.ForeignKey(CostPerDay, blank=True, null=True)

admin.site.register(Availability)


class Review(models.Model):
    vehicle = models.ForeignKey(Vehicle)
    person = models.ForeignKey(Person)
    comment = models.CharField(max_length=500)
    rating = models.IntegerField()

admin.site.register(Review)


class Booking(models.Model):
    renter = models.ForeignKey(Person)
    vehicle = models.ForeignKey(Vehicle)
    cost = models.ForeignKey(CostPerDay)
    startTime = models.DateTimeField()
    endTime = models.DateTimeField()
    confirmedByOwner = models.BooleanField(default=False)
    rejectedByOwner = models.BooleanField(default=False)
    canceledByRenter = models.BooleanField(default=False)
    hasBond = models.BooleanField(default=True)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    def bookings_for_user(self, user):
        bookings = Booking.objects.filter(renter=user)
        b = [{'b': b, 'r': Review.objects.filter(vehicle=b.vehicle).last()} for b in bookings]
        return b

    def sendBookingRequestMessage(self):
        message = Message()
        message.sender = self.renter
        message.receiver = self.vehicle.owner
        message.timestamp = datetime.now()
        message.message = " requested to rent " + str(self.vehicle.licensePlate) + " between "+str(self.startTime)+" and "+str(self.endTime)
        message.isBookingRequestMessage = True
        message.referencesBooking = self
        message.save()

        # And send an email.
        # server = smtplib.SMTP('box.browncowapps.com.au')
        # server.ehlo()
        # server.starttls()
        # server.login('swiftrides@mattlavender.email', 'swiftridesimap')
        # server.sendmail('swiftrides@mattlavender.email',
        #                 self.vehicle.owner.user.email,
        #                 'From: swiftrides@mattlavender.email\r\nTo: ' + str(self.vehicle.owner.user.email) + '\r\nSubject: New rent request\r\nYou\'ve received a request from a user to rent one of your vehicles')
        # server.quit()
        #
        message = 'You\'ve received a request from a user to rent one of your vehicles'
        send_mail(
            'New rent request',
            message,
            settings.EMAIL_HOST_USER,
            ['ponir.techlogicians@gmail.com',self.vehicle.owner.user.email],
        )

    def sendBookingAcceptRejectMessage(self):
        message = Message()
        message.sender = self.vehicle.owner
        message.receiver = self.renter
        message.timestamp = timezone.now()

        status = None
        if self.rejectedByOwner:
            status = 'rejected'
        if self.confirmedByOwner:
            status = 'accepted'

        start = self.startTime+timedelta(hours=8)
        start = start.strftime('%Y-%m-%d')
        end = self.endTime+timedelta(hours=8)
        end = end.strftime('%Y-%m-%d')
        message.message =" "+status +' '+ str(self.vehicle.licensePlate) + " between "+str(start)+" and "+str(end)
        message.isBookingRequestMessage = True
        message.referencesBooking = self
        message.readTimestamp = timezone.now()
        message.save()

        message = 'Your request for booking vehicle '+self.vehicle.licensePlate + ' has been '+status+' between '+str(start)+' and '+str(end)
        send_mail(
            'Rent Booking',
            message,
            settings.EMAIL_HOST_USER,
            ['ponir.techlogicians@gmail.com',self.renter.user.email],
        )

admin.site.register(Booking)


class Message(models.Model):
    sender = models.ForeignKey(Person, related_name="+")
    receiver = models.ForeignKey(Person, related_name="+")
    timestamp = models.DateTimeField()
    readTimestamp = models.DateTimeField(blank=True, null=True)
    message = models.CharField(max_length=500)

    isBookingRequestMessage = models.BooleanField(default=False)
    referencesBooking = models.ForeignKey(Booking, null=True, blank=True, default=None)

    def __unicode__(self):
        return unicode(self.sender) + " -> " + unicode(self.receiver) + ": " + unicode(self.message)

    def originator(self, aUser):
        print "originator"
        print aUser
        return True if self.sender.user.email == aUser.user.email else False

    def booking_request(self,user):
        messages = Message.objects.filter(Q(sender=user) | Q(receiver=user)).filter(isBookingRequestMessage=True).values('referencesBooking_id').distinct()
        msgs = []
        for message in messages:
            msg = Message.objects.filter(referencesBooking_id=message['referencesBooking_id']).last()
            msgs.append(msg)
        return msgs

    def user_messages(self,user):
        messages = Message.objects.filter(Q(sender=user) | Q(receiver=user)).filter(isBookingRequestMessage=False)
        contacted_users = []
        for message in messages:
            if message.sender_id not in contacted_users:
                if message.sender_id != user.id:
                    contacted_users.append(message.sender_id)
            if message.receiver_id not in contacted_users:
                if message.receiver_id != user.id:
                    contacted_users.append(message.receiver_id)

        messages = []
        for contacted_user in contacted_users:
            message = Message.objects.filter(Q(sender=user) | Q(receiver=user)).filter(Q(sender=contacted_user) | Q(receiver=contacted_user)).filter(isBookingRequestMessage=False).last()
            messages.append(message)

        return messages

    def threads_for_user(self, user):
        messages = list(Message.objects.filter(Q(sender=user) | Q(receiver=user)))
        threads = dict()
        for m in messages:
            otheruser = None

            if m.sender.id == m.receiver.id:
                continue

            if m.sender.id is not user.id:
                otheruser = m.sender

            elif m.receiver.id is not user.id:
                otheruser = m.receiver

            if not otheruser:
                otheruser = user

            if otheruser.user.email not in threads:
                threads[otheruser.user] = list()

            threads[otheruser.user].append({
                'sender': m.sender,
                'receiver': m.receiver,
                'receiver_name':m.receiver.displayName,
                'receiver_id':m.receiver.id,
                'receiver_image':m.receiver.profilePicture.url,
                'message': m.message,
                'isBookingRequestMessage':'true' if m.isBookingRequestMessage is True else 'false' ,
                'read': 'true' if m.readTimestamp is not None else 'false',
                'timestamp': str(m.timestamp),
                'originator': 'true' if m.sender.user.email == user.user.email else 'false'})

        return threads



admin.site.register(Message)


class Verification(models.Model):
    code = models.CharField(max_length=30)
    expires = models.DateTimeField()
    used = models.BooleanField(default=False)
    verified = models.BooleanField(default=False)
    verifiedTime = models.DateTimeField(null=True, blank=True)
    person = models.ForeignKey(Person)

    VERIFYING_CHOICE_EMAIL = 'EMAIL'
    VERIFYING_CHOICE_DATEOFBIRTH = 'DATEOFBIRTH'
    VERIFYING_CHOICE_ADDRESS = 'ADDRESS'
    VERIFYING_CHOICE_PHONE = 'PHONE'
    VERIFYING_CHOICE_NAME = 'NAME'
    VERIFYING_CHOICE_VEHICLEDOCUMENTS = 'VEHICLEDOCUMENTS'
    VERIFYING_CHOICE_PERSONDOCUMENTS = 'PERSONDOCUMENTS'
    verifying_choices = ((VERIFYING_CHOICE_EMAIL, 'Email'),
                         (VERIFYING_CHOICE_DATEOFBIRTH, 'Date of Birth'),
                         (VERIFYING_CHOICE_ADDRESS, 'Address'),
                         (VERIFYING_CHOICE_PHONE, 'Phone'),
                         (VERIFYING_CHOICE_NAME, 'Name'),
                         (VERIFYING_CHOICE_VEHICLEDOCUMENTS, 'Vehicle Documents'),
                         (VERIFYING_CHOICE_PERSONDOCUMENTS, 'Person Documents'),)
    verifying = models.CharField(max_length=50, choices=verifying_choices)

    def generate_code(self, person=None):
        hashing = "RentUrCar Static Salt"
        try:
            if person is not None:
                hashing += str(person.id) + str(person.user.email)
        except:
            hashing += str(datetime.now())

        return hashlib.sha512(hashing).hexdigest()[:30]

    def __unicode__(self):
        return str(self.person) + "(" + self.verifying + "): " + str(self.code)

    def save(self, *args, **kwargs):
        if self.verifying is self.VERIFYING_CHOICE_EMAIL:
            self.sendEmail(self.person.user.email, '')

        # if self.verifying is self.VERIFYING_CHOICE_PHONE:
        #     self.sendSMS('0413 099 230', 'Verify your phone number by tapping this link: http://localhost:8000/user/verify/a/' + str(self.code))

        super(Verification, self).save(*args, **kwargs)

    def sendSMS(self, phoneNumber, message):
        tapi_consumer_key = 'Qly5GRsOvunzJqYNDumKUaRABZAsYsPs'
        tapi_consumer_secret = 'besllvn3vJE11GbG'

        auth_request = requests.post("https://api.telstra.com/v1/oauth/token", data={
            'client_id': tapi_consumer_key,
            'client_secret': tapi_consumer_secret,
            'grant_type': 'client_credentials',
            'scope': 'SMS'})

        auth_response = auth_request.json()
        if 'access_token' not in auth_response:
            # Uhoh.
            print("An error occured with the Auth Request.")
            return False

        else:
            # Send the SMS.
            smsdata = {'to': phoneNumber, 'body': message}
            headers = {'Content-type': 'application/json', 'Authorization': 'Bearer ' + str(auth_response['access_token'])}
            url = "https://api.telstra.com/v1/sms/messages"

            try:
                req = urllib2.Request(url, headers=headers, data=json.dumps(smsdata))
                msg = urllib2.urlopen(req)
                if msg.getcode() < 200 or msg.getcode() > 299:
                    print "ERROR: " + str(msg.getcode())
                    return False

                else:
                    return True

            except urllib2.URLError:
                print "Exception"
                return False

    def sendEmail(self, emailAddress, message):
        print("Here is email")
        # print(emailAddress)
        # server = smtplib.SMTP('smtp.gmail.com',587)
        # server.ehlo()
        # server.starttls()
        # server.login('ponir.techlogician@gmail.com', 't3chlogicians')
        # server.sendmail('ponir.techlogician@gmail.com', 'ponir.techlogicians@gmail.com', 'From: ponir.techlogicians@gmail.com\r\nTo: ponir.techlogicians@gmail.com\r\nSubject: Confirm your SwyftRides account\r\nVerify your email address by clicking this link: http://127.0.0.1:8000/user/verify/a/' + str(self.code))
        # #server.sendmail('shakeel@techlogicians.com', emailAddress, 'From: swiftrides@mattlavender.email\r\nTo: swiftrides@mattlavender.email\r\nSubject: Confirm your SwyftRides account\r\nVerify your email address by clicking this link: http://127.0.0.1:8000/user/verify/a/' + str(self.code))
        # server.quit()
        message = 'Confirm your SwyftRides account\r\nVerify your email address by clicking this link: http://techlogiciansdjango.pythonanywhere.com/user/verify/a/' + str(self.code)
        send_mail(
            'Account Confirmation - SwyftRides',
            message,
            settings.EMAIL_HOST_USER,
            ['ponir.techlogicians@gmail.com',emailAddress],
        )

admin.site.register(Verification)
