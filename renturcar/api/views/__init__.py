from django.shortcuts import render, render_to_response, redirect
from django.db.models import Q
from api.models import *
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.models import User
from django.contrib.auth.decorators import *
from datetime import datetime
import pendulum
from django.db.models import Avg, Count
from django.contrib.auth.decorators import *
from django.http import Http404, JsonResponse
from django.shortcuts import get_object_or_404


from api.forms import ContactForm
from django.template.loader import get_template
from django.core.mail import EmailMessage
from django.template import Context


from auth import *
from user import *
from search import *
from django.contrib import messages
from django.utils import timezone
def home_view(request):
    return render(request, 'home.html')

def terms_and_conditions_view(request):
    return render(request,'terms_and_conditions.html')

def car_view(request, carid):

    try:
        int(carid)

    except ValueError:
        raise Http404()

    car = Vehicle.objects.filter(id=carid).first()
    if not car:
        return HttpResponse

    else:
        car.increase_number_of_view()
        images = VehiclePhoto.objects.filter(vehicle=car)
        properties = VehicleProperty.objects.filter(vehicle=car)
        reviews = Review.objects.filter(vehicle=car)
        return render(request, 'car.html', context={'car': car, 'images': images, 'properties': properties, 'reviews': reviews})


def car_availability(request, carid, startDate, endDate):

    try:
        int(carid)

    except ValueError:
        raise Http404()

    car = Vehicle.objects.filter(id=carid).first()
    if not car:
        raise Http404()

    s = pendulum.parse(startDate, tz='Australia/West', strict=True)
    e = pendulum.parse(endDate, strict=True)

    ret = {}
    for day in pendulum.period(s, e).range('days'):
        ret[day.to_date_string()] = car.vehicleAvailableOnDate(day)

    return JsonResponse(ret)

def car_request_view(request, carid, requestid=None, action=None):

    if request.method == 'POST':
        try:
            int(carid)

        except ValueError:
            raise Http404

        car = get_object_or_404(Vehicle, id=carid)
        # Do the booking request.
        booking = Booking()
        booking.renter = request.user.person
        booking.vehicle = car
        booking.cost = car.defaultCost

        booking.startTime = pendulum.timezone("Australia/West").convert(pendulum.parse(request.POST['rentfromdate'], tz='Australia/West')).date()
        booking.endTime = pendulum.timezone("Australia/West").convert(pendulum.parse(request.POST['renttodate'], tz='Australia/West')).date()

        carAvailable = True

        for bday in pendulum.period(booking.startTime, booking.endTime).range('days'):
            if not car.vehicleAvailableOnDate(bday):
                carAvailable = False


        if not carAvailable:
             messages.add_message(request, messages.WARNING,'Car is not available on given dates.Please Try again with another dates.')

        if carAvailable:
            booking.save()
            booking.sendBookingRequestMessage()
            messages.add_message(request, messages.SUCCESS,'Booking has been successfully placed but it need to be confirmed by the owner.You will be notified soon by the owner')

        return redirect("/car/" + str(car.id))

    else:
        try:
            int(requestid)

        except ValueError:
            raise Http404

        booking = get_object_or_404(Booking, id=requestid)
        message = Message.objects.get(referencesBooking_id=requestid)
        if action == 'accept':
            booking.confirmedByOwner = True
            message.readTimestamp = timezone.now()
            availability = Availability()
            availability.startTime = booking.startTime
            availability.endTime = booking.endTime
            availability.cost = booking.cost
            availability.vehicle = booking.vehicle
            availability.save()
        elif action == 'decline':
            booking.rejectedByOwner = True
            message.readTimestamp = timezone.now()

        elif action == 'cancel':
            booking.canceledByRenter = True

        else:
            raise Http404

        message.save()
        booking.save()
        booking.sendBookingAcceptRejectMessage()
        return redirect("/user/booking-inbox/" + str(requestid))


def send_message_view(request):
    if request.method != 'POST':
        raise Http404

    if 'referrer' not in request.GET:
        raise Http404

    # We have a referrer and this was a POST.
    text = request.POST.get('text', None)
    fromUser = request.user.person
    toUser = get_object_or_404(Person, id=request.POST.get('to', None))

    if toUser.id == fromUser.id:
        raise Http404

    message = Message()
    message.sender = fromUser
    message.receiver = toUser
    message.timestamp = datetime.now()
    message.message = text
    message.save()

    return redirect(request.GET['referrer'])


def contact_view(request):

    form_class = ContactForm

    if request.method == 'POST':
        form = form_class(data=request.POST)

        if form.is_valid():
            contact_name = form.cleaned_data['contact_name']
            contact_email = form.cleaned_data['contact_email']
            form_content = form.cleaned_data['content']

            template = get_template('contact_template.txt')

            context = Context({
                'contact_name': contact_name,
                'contact_email': contact_email,
                'form_content': form_content,
            })

            content = template.render(context)

            email = EmailMessage(
                'New contact form submission',
                content,
                'Your website <hi@swiftrydes.com>',
                ['youremail@gmail.com'],
                headers={'Reply-To': contact_email}
            )

            email.send()
            return redirect('contact_view')

    return render(request, 'contact.html', {'form': form_class})


@login_required
def new_listing_view(request):

    if request.method == 'POST':

        context = {}
        valid = True
        if 'vehicle' not in request.session:
            request.session['vehicle'] = {}

        if 'STEPONE' not in request.session:
            if 'body' not in request.POST:
                valid = False
                context['step'] = None

            else:
                request.session['vehicle']['body'] = request.POST['body']
                request.session['STEPONE'] = True
                context['step'] = 'STEPONE'

                makes = [
                    {'name': 'Toyota', 'id': 1},
                    {'name': 'Subaru', 'id': 2},
                    {'name':'Honda','id':3}
                ]
                context['makes'] = makes

                models = [
                    {'name': 'WRX', 'id': 1},
                    {'name': 'Tundra', 'id': 2},
                    {'name': 'Corolla', 'id': 3},
                    {'name': 'Land Cruiser', 'id': 4},
                    {'name': 'Camry', 'id': 5},
                    {'name': 'Highlander', 'id': 6},
                    {'name': 'RAV4', 'id': 7},
                    {'name': 'Avalon', 'id': 8},
                    {'name': 'Accord', 'id': 9},
                    {'name': 'Civic', 'id': 10},
                    {'name': 'Pilot', 'id': 11},
                    {'name': 'Fit', 'id': 12},
                    {'name': 'Insight', 'id': 13},
                    {'name': 'Legacy', 'id': 14},
                    {'name': 'Impreza', 'id': 15},
                    {'name': 'Outback', 'id': 16}
                ]
                context['models'] = models

            return render(request, 'new-listing.html', context=context)

        elif 'STEPONE' in request.session and 'STEPTWO' not in request.session:
            req_fields = ['l', 'p', 'm', 'ml', 'k', 'c', 't', 'cm','lpn']
            for f in req_fields:
                if f not in request.POST or request.POST[f] is None or request.POST[f] == '':
                    context[f] = 'This is a required field.'
                    valid = False

                else:
                    if 'prepopulate' not in context:
                        context['prepopulate'] = {}

                    context['prepopulate'][f] = request.POST[f]

            if not valid:
                context['step'] = 'STEPONE'

                makes = [
                    {'name': 'Toyota', 'id': 1},
                    {'name': 'Subaru', 'id': 2},
                    {'name':'Honda','id':3}
                ]
                context['makes'] = makes

                models = [
                    {'name': 'WRX', 'id': 1},
                    {'name': 'Tundra', 'id': 2},
                    {'name': 'Corolla', 'id': 3},
                    {'name': 'Land Cruiser', 'id': 4},
                    {'name': 'Camry', 'id': 5},
                    {'name': 'Highlander', 'id': 6},
                    {'name': 'RAV4', 'id': 7},
                    {'name': 'Avalon', 'id': 8},
                    {'name': 'Accord', 'id': 9},
                    {'name': 'Civic', 'id': 10},
                    {'name': 'Pilot', 'id': 11},
                    {'name': 'Fit', 'id': 12},
                    {'name': 'Insight', 'id': 13},
                    {'name': 'Legacy', 'id': 14},
                    {'name': 'Impreza', 'id': 15},
                    {'name': 'Outback', 'id': 16}
                ]
                context['models'] = models

                return render(request, 'new-listing.html', context)

            else:
                # Save.
                request.session['vehicle']['location'] = request.POST['l']
                request.session['vehicle']['price'] = request.POST['p']
                request.session['vehicle']['make'] = request.POST['m']
                request.session['vehicle']['model'] = request.POST['ml']
                request.session['vehicle']['km'] = request.POST['k']
                request.session['vehicle']['colour'] = request.POST['c']
                request.session['vehicle']['comments'] = request.POST['cm']
                request.session['vehicle']['title'] = request.POST['t']
                request.session['vehicle']['year'] = request.POST['y']
                request.session['vehicle']['cost'] = request.POST['p']
                request.session['vehicle']['licence_plate'] = request.POST['lpn']

                request.session['STEPTWO'] = True
                context['step'] = 'STEPTWO'

                lineOne=[]
                lineTwo=''
                country=''
                postal_code=''
                if 'street_number' in request.POST:
                    street_number = request.POST['street_number']
                    lineOne.append(street_number)

                if 'route' in request.POST:
                    route = request.POST['route']
                    lineOne.append(route)

                line1 = ''
                line1 = ' '.join(lineOne)
                if 'locality' in request.POST:
                    lineTwo = request.POST['locality']
                if 'country' in request.POST:
                    country = request.POST['country']
                if 'postal_code' in request.POST:
                    postal_code = request.POST['postal_code']



                v = Vehicle()
                v.make = request.session['vehicle']['make']
                v.model = request.session['vehicle']['model']
                v.year = request.session['vehicle']['year']
                v.colour = request.session['vehicle']['colour']
                v.licensePlate = request.session['vehicle']['licence_plate']
                v.vin = '123abc'
                v.address = address_get_or_create(line1,lineTwo,country,postal_code)
                v.owner = request.user.person
                v.comments = request.session['vehicle']['comments']
                v.rules = ''
                v.minRentDays = 0
                v.maxRentDays = 0
                v.advanceNotice = 0
                v.preparationTime = 0
                c = CostPerDay()
                c.rentalCost = request.session['vehicle']['cost']
                c.commissionCost = 0.0
                c.save()
                v.defaultCost = c
                v.bodyType = BodyType.objects.all().first()
                v.save()
                request.session['vehicle_id'] = v.id
                for f in request.FILES:
                    vp = VehiclePhoto()
                    vp.photo = request.FILES[f]
                    vp.vehicle = v
                    vp.save()

                #return redirect("/car/" + str(v.id))
                return render(request, 'new-listing.html', context=context)

        elif 'STEPTWO' in request.session:
            # req_fields = ['fd','bd','fi','pi']
            req_fields = ['fi','pi']
            for f in req_fields:
                if f not in request.FILES or request.FILES[f] is None or request.FILES[f] == '':
                    context[f] = 'This is a required field.'
                    valid = False

            if not valid:
                context['step'] = 'STEPONE'

                makes = [{'name': 'Toyota', 'id': 1}, {'name': 'Subaru', 'id': 2}]
                context['makes'] = makes

                models = [{'name': 'WRX', 'id': 1}]
                context['models'] = models

                return render(request, 'new-listing.html', context)
            else:
                # driver_license_front = request.FILES['fd']
                # driver_license_back = request.FILES['bd']
                print("----------------------------------")
                insurance_3fs=request.FILES['fi']
                private_insurane = request.FILES['pi']
                if 'vehicle_id' in request.session:
                    print("HERE")
                    vehicle = Vehicle.objects.get(id=request.session['vehicle_id'])
                    vd= VehicleDocument(vehicle=vehicle,document=private_insurane)
                    vd.save()
                    print("3fs")
                    print(insurance_3fs)

                    vi= VehicleDocument(vehicle=vehicle,document=insurance_3fs)
                    vi.save()
                    #
                    # pdf=PersonDocument(person=request.user.person,document=driver_license_front)
                    # pdf.save()
                    # pdb=PersonDocument(person=request.user.person,document=driver_license_back)
                    # pdb.save()
                    return redirect("/car/" + str(vehicle.id))
        else:
            # Something went wrong, send the user back to the first page.
            return render(request, 'new-listing.html')

    elif request.method == 'GET':
        for a in ['vehicle', 'STEPONE', 'STEPTWO', 'STEPTHREE']:
            if a in request.session:
                request.session.pop(a)

        return render(request, 'new-listing.html', context={'bodyTypes': BodyType.objects.all()})


def address_get_or_create(lineOne,city,country,post_code):
    address_obj = None
    address = Address.objects.filter(lineOne=lineOne,lineTwo=city,country=country,postCode=post_code)
    if address.exists():
        address_obj = address.first()
    else:
        address_obj = Address()
        address_obj.lineOne=lineOne
        address_obj.lineTwo=city
        address_obj.country=country
        address_obj.postCode=post_code
        address_obj.save()
    return address_obj

def requirements_view(request):
    return render(request, 'requirements.html')
