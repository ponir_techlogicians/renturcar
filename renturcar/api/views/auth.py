from django.shortcuts import render, redirect, render_to_response
from api.models import *
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.models import User
from django.utils import timezone, dateparse
import pendulum
from django.contrib import messages

def login_view(request):

    goto = request.GET.get('next', '/')

    LOGIN_ERROR_STRING = 'kLoginErrorString'

    if request.user.is_authenticated():
        return redirect('/')

    if request.method == 'POST':
        if 'x' not in request.POST or 'y' not in request.POST:
            request.session[LOGIN_ERROR_STRING] = "Incorrect email or password"
            return redirect(goto)

        else:
            user = authenticate(username=request.POST['x'], password=request.POST['y'])
            if user is not None:
                if user.person.is_email_verified():
                    login(request, user)
                # return redirect(request.META.get('HTTP_REFERER', '/'))
                    return redirect(goto)
                else:
                    request.session[LOGIN_ERROR_STRING] = "You havent confirmed your email yet."
                    return redirect(login_view)


            else:
                request.session[LOGIN_ERROR_STRING] = "Incorrect email or password"
                return redirect(login_view)

    else:
        error = None
        if LOGIN_ERROR_STRING in request.session:
            error = request.session[LOGIN_ERROR_STRING]
            request.session[LOGIN_ERROR_STRING] = None

        return render(request, 'login.html', context={'error': error, 'goto': goto})


def logout_view(request):
    logout(request)
    return redirect('/')


def signup_view(request):
    if request.method == 'POST':

        if 'quick' in request.POST:
            return quick_signup(request)

        context = {}
        valid = True
        if 'person' not in request.session:
            request.session['person'] = {}
            request.session['person']['user'] = {}
            request.session['person']['address'] = {}

        context['person'] = request.session['person']

        if 'STEPONE' not in request.session:
            # Validate the received input from step one of signup.
            # Required fields:
            req_fields = ["fn", "dob", "x"]
            for f in req_fields:
                print "Checking for " + str(f)
                if f not in request.POST or request.POST[f] is None or request.POST[f] == '':
                    # Return error.
                    print " Not found"
                    context[f] = " is a required field."
                    valid = False

                else:
                    if 'prepopulate' not in context:
                        context['prepopulate'] = {}

                    context['prepopulate'][f] = request.POST[f]

            # Non required fields:
            nreq_fields = ["ln", "ph"]
            for f in nreq_fields:
                print "Checking for " + str(f)
                if f in request.POST:
                    if 'prepopulate' not in context:
                        context['prepopulate'] = {}

                    context['prepopulate'][f] = request.POST[f]

            if "x" in request.POST and request.POST["x"] is not None:
                # Check this email address hasn't been used before.
                users = MyUser.objects.filter(email=request.POST["x"])
                if users.count() == 1:
                    context["x"] = " has already been associated with an account."
                    valid = False

                elif users.count() > 1:
                    print "Houston, we have a problem."
                    valid = False

            if "dob" in request.POST and request.POST["dob"] is not None:
                if dateparse.parse_date(request.POST['dob']) is None:
                    print "date is none"
                    valid = False
                    context["dob"] = " was an invalid format."
                    context['prepopulate']["dob"] = ""

            # If nothing was invalid, head on to step two.
            if valid:
                # Save the users progress
                request.session['person']['firstname'] = request.POST['fn']
                request.session['person']['user']['email'] = request.POST['x']
                request.session['person']['dob'] = request.POST['dob']
                if 'ph' in request.POST:
                    request.session['person']['phone'] = request.POST['ph']

                if 'ln' in request.POST:
                    request.session['person']['lastname'] = request.POST['ln']

                request.session['STEPONE'] = True
                context["step"] = "STEPONE"

            else:
                context["step"] = None

            return render(request, 'signup.html', context)

        elif 'STEPONE' in request.session and 'STEPTWO' not in request.session:
            # Validate the received input.
            # Required fields:
            req_fields = ["addr1", "sub", "pc", "st", "c"]
            for f in req_fields:
                print "Checking for " + str(f)
                if f not in request.POST or request.POST[f] is None or request.POST[f] == '':
                    # Return error.
                    print " Not found"
                    context[f] = " is a required field."
                    valid = False

                else:
                    if 'prepopulate' not in context:
                        context['prepopulate'] = {}

                    context['prepopulate'][f] = request.POST[f]

            # Non required fields:
            nreq_fields = ["addr2", "addr3"]
            for f in nreq_fields:
                print "Checking for " + str(f)
                if f in request.POST:
                    if 'prepopulate' not in context:
                        context['prepopulate'] = {}

                    context['prepopulate'][f] = request.POST[f]

            # If nothing was invalid, head on to step three.
            if valid:
                request.session['person']['address']['lineOne'] = request.POST['addr1']
                request.session['person']['address']['country'] = request.POST['c']
                request.session['person']['address']['postCode'] = request.POST['pc']
                request.session['person']['address']['state'] = request.POST['st']
                request.session['person']['address']['suburb'] = request.POST['sub']
                if 'addr2' in request.POST:
                    request.session['person']['address']['lineTwo'] = request.POST['addr2']

                if 'addr3' in request.POST:
                    request.session['person']['address']['lineThree'] = request.POST['addr3']
                request.session['STEPTWO'] = True
                context["step"] = "STEPTWO"

            else:
                context["step"] = "STEPONE"

            return render(request, 'signup.html', context)

        elif 'STEPTWO' in request.session and 'STEPTHREE' not in request.session:
            # Validate the response
            if 'y' not in request.POST or request.POST['y'] == '' or 'y1' not in request.POST or request.POST['y1'] == '':
                context['y1'] = " is a required field"
                context["step"] = "STEPTWO"
                return render(request, 'signup.html', context)

            else:
                # Save the user.
                u = MyUser()
                u.email = request.session['person']['user']['email']
                u.set_password(request.POST['y'])
                u.save()
                p = Person()
                p.user = u
                p.firstName = request.session['person']['firstname']
                if 'lastname' in request.session['person']:
                    p.lastName = request.session['person']['lastname']
                p.dateOfBirth = dateparse.parse_date(request.session['person']['dob'])  # pendulum.parse(request.session['person']['dob'])
                a = Address()
                a.lineOne = request.session['person']['address']['lineOne']
                if 'lineTwo' in request.session['person']['address']:
                    a.lineTwo = request.session['person']['address']['lineTwo']
                a.country = request.session['person']['address']['country']
                a.postCode = request.session['person']['address']['postCode']
                a.save()
                p.address = a
                if 'phone' in request.session['person']:
                    p.phone = request.session['person']['phone']
                p.save()
                login(request, u)
                return redirect("/user")

    elif request.method == 'GET':
        request.session.clear()
        return render(request, 'signup.html')


def quick_signup(request):
    # email, first, last, password, dob
    req = ['x', 'fName', 'y', 'dob']
    for a in req:
        if a not in request.POST:
            return redirect("/")

    # Got everything, now process.
    u = MyUser()
    u.email = request.POST['x']
    u.set_password(request.POST['y'])
    u.save()
    p = Person()
    p.user = u
    p.firstName = request.POST['fName']
    if 'lName' in request.POST:
        p.lastName = request.POST['lName']
    p.dateOfBirth = request.POST["dob"]
    p.save()

    #login(request, u)

    #return redirect("/user")
    messages.add_message(request, messages.INFO, 'A mail has been sent to your email address. please confirm your account.')
    return redirect("/confirmation")