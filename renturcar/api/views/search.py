from django.shortcuts import render, render_to_response, redirect
from django.db.models import Q
from api.models import *
from datetime import datetime
import pendulum
from django.db.models import Avg, Count

#
# Basic search includes: location, date and body type.
# These use the parameters: l, d and b, respectively.
# A refiened/advanced search also includes: end date, low price, high price, make, model, year and air conditioning
# These use the params: de, pf, pt, m, ml, y and a, respectively.
# A search will & these queries to find something that matches all of them.
#
# A 'similar to' result set should include objects that match the majority of the options:
# Try for vehicles that match the requirements but are further from the location specifed,
# push prices out a little,
# same make and body type but different model,
# a year or two either side, etc.
# The resulting queries should then be | to create a single query that is run.
#


def advanced_query(search_params):
    qFilter = None
    optionQ = None
    if 'd' in search_params:
        sdate=search_params['d']
        edate=search_params['de']
        if 'de' in search_params:
            optionQ = ~Q(availability__in=Availability.objects.filter(startTime__date__gte=sdate, endTime__date__lte=edate))

    for searchOption in ['d','de', 'pf', 'pt', 'm', 'ml', 'y', 'a']:
        if searchOption in search_params and search_params[searchOption] is not u'':

            # if searchOption is 'de':
            #     searchDate = datetime.now()
            #     tz = pendulum.timezone("Australia/West")
            #     qDate = tz.convert(searchDate).date()
            #     optionQ = Q(availability__in=Availability.objects.filter(endTime__date__gt=qDate))


            if searchOption is 'pf':
                optionQ = Q(defaultCost__rentalCost__gte=float(search_params['pf']))

            elif searchOption is 'pt':
                optionQ = Q(defaultCost__rentalCost__lte=float(search_params['pt']))

            elif searchOption is 'm':
                optionQ = Q(make__icontains=search_params['m'])

            elif searchOption is 'ml':
                optionQ = Q(model__icontains=search_params['ml'])

            elif searchOption is 'y':
                optionQ = Q(year__icontains=search_params['y'])

            elif searchOption is 'a':
                # TODO:
                pass

            if optionQ is not None:
                if qFilter is None:
                    qFilter = optionQ

                else:
                    qFilter = qFilter & optionQ

    return qFilter


def build_query(search_params):
    if 'rf' in search_params:
        qFilter = advanced_query(search_params)

    else:
        qFilter = None

    for searchOption in ['l', 'd', 'b']:
        if searchOption in search_params and search_params[searchOption] is not u'':
            optionQ = None
            q = None
            if searchOption is 'l':
                input_line_1 = []
                if 'street_number' in search_params:
                    street_number = search_params['street_number']
                    if street_number != '':
                        input_line_1.append(street_number)
                if 'route' in search_params:
                    route = search_params['route']
                    if route != '':
                        input_line_1.append(route)
                lineOne = None
                if len(input_line_1)>0:
                    input_line_1 = ' '.join(input_line_1)
                    lineOne = input_line_1
                    q = Q(address__lineOne__icontains=lineOne)

                lineTwo = None
                if 'locality' in search_params:
                    lineTwo = search_params['locality']
                    if q is not None:
                        if lineTwo != '':
                            q = q | Q(address__lineTwo__icontains=lineTwo)
                    else:
                        if lineTwo != '':
                            q = Q(address__lineTwo__icontains=lineTwo)

                country = None
                if 'country' in search_params:
                    country = search_params['country']
                    if q is not None:
                        if country != '':
                            q = q | Q(address__country__icontains=country)
                    else:
                        if country != '':
                            q = Q(address__country__icontains=country)

                postal_code = None
                if 'postal_code' in search_params:
                    postal_code = search_params['postal_code']
                    if q is not None:
                        if postal_code != '':
                            q = q | Q(address__postCode__icontains=postal_code)
                    else:
                        if postal_code != '':
                            q = Q(address__postCode__icontains=postal_code)


                optionQ =q

            elif searchOption is 'b':
                body = BodyType.objects.filter(name__iexact=search_params['b']).first()
                if body:
                    optionQ = Q(bodyType=body)
                else:
                    optionQ = Q(make=search_params['b'])|Q(model=search_params['b'])

            if optionQ is not None:
                if qFilter is None:
                    qFilter = optionQ

                else:
                    qFilter = qFilter & optionQ

    return qFilter


def search_view(request):
    print request.GET
    if 'q' not in request.GET:
        title = 'Search results'
        results = list()
        results_count = 0

    else:
        title = 'Results for `' + str(request.GET['q']) + '`'

        qFilter = build_query(request.GET)
        results = list()
        if qFilter is not None:
            vehicles = Vehicle.objects.filter(qFilter).annotate(rating=Avg('review__rating')).annotate(numratings=Count('review'))
            for v in vehicles:
                if v.showInSearch():
                    results.append(v)

        else:
            results = list()

        print len(results)
        if len(results) > 0:
            results_count = str(len(results)) + ' results found'

        else:
            results_count = 'No results found, try something different'

    prefill = {}
    for var in ['b', 'd', 'l']:
        if var in request.GET:
            prefill[var] = request.GET[var]

    return render(request, 'search.html', {
        'title': title,
        'results': results,
        'results_count': results_count,
        'related_searches': [],
        'prefill': prefill
    })
