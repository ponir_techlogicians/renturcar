from django.shortcuts import render, render_to_response, redirect, HttpResponse
from api.models import *
from django.contrib.auth.decorators import *
from django.utils import timezone, dateparse
import pendulum
from django.template import RequestContext
from django.shortcuts import get_object_or_404
from django.http import Http404
from django.contrib import messages
from django.contrib.auth import update_session_auth_hash
from django.contrib.auth import authenticate, login, logout
def post_var(var):
    if type(var) == unicode:
        if var != '':
            return True

        return False

    else:
        # Not unicode
        print "Unhandled type in post_var (user.py)"
        return True

import json
@login_required
def user_account_index_view(request):

    if request.method == 'POST':
        needsSaving = False

        if 'fn' in request.POST and post_var(request.POST['fn']) and request.POST['fn'] != request.user.person.firstName:
            request.user.person.firstName = request.POST['fn']
            needsSaving = True

        if 'ln' in request.POST and post_var(request.POST['ln']) and request.POST['ln'] != request.user.person.lastName:
            request.user.person.lastName = request.POST['ln']
            needsSaving = True

        if 'dn' in request.POST and post_var(request.POST['dn']) and request.POST['dn'] != request.user.person.displayName:
            request.user.person.displayName = request.POST['dn']
            needsSaving = True

        if 'g' in request.POST and request.POST['g'] is not None and request.POST['g'] != request.user.person.gender:
            request.user.person.gender = request.POST['g']
            needsSaving = True

        if 'dob' in request.POST and post_var(request.POST['dob']) and dateparse.parse_date(request.POST['dob']) != request.user.person.dateOfBirth:
            request.user.person.dateOfBirth = dateparse.parse_date(request.POST['dob'])
            needsSaving = True

        if 'x' in request.POST and post_var(request.POST['x']) and request.POST['x'] != request.user.email and request.POST['x'] != request.user.newEmail:
            request.user.newEmail = request.POST['x']
            needsSaving = True

        if 't' in request.POST and post_var(request.POST['t']) and request.POST['t'] != request.user.person.phone:
            request.user.person.phone = request.POST['t']
            needsSaving = True

        if 'addr1' in request.POST and post_var(request.POST['addr1']):
            #request.user.person.address.lineOne = request.POST['addr1']
            lineOne=[]
            lineTwo=''
            country=''
            postal_code=''
            if 'street_number' in request.POST:
                street_number = request.POST['street_number']
                lineOne.append(street_number)

            if 'route' in request.POST:
                route = request.POST['route']
                lineOne.append(route)

            line1 = ''
            line1 = ' '.join(lineOne)
            lineOne_old = ''
            if 'lineOne' in request.POST:
                lineOne_old = request.POST['lineOne']
            if 'locality' in request.POST:
                lineTwo = request.POST['locality']
            if 'country' in request.POST:
                country = request.POST['country']
            if 'postal_code' in request.POST:
                postal_code = request.POST['postal_code']
            if line1!=lineOne_old or lineTwo!=request.user.person.address.lineTwo or country!=request.user.person.address.country or postal_code!=request.user.person.address.postCode:
                request.user.person.address=address_get_or_create(line1,lineTwo,country,postal_code)
            needsSaving = True

        if needsSaving:
            request.user.person.save()
            request.user.save()

        return redirect(user_account_index_view)

    else:
        addressArr=[]
        address=None
        if request.user.person.address:
            if request.user.person.address.lineOne != None and request.user.person.address.lineOne != ' ':
                addressArr.append(request.user.person.address.lineOne)
            addressArr.append(request.user.person.address.lineTwo)
            addressArr.append(request.user.person.address.country)
            addressArr.append(request.user.person.address.postCode)
            address = ','.join(addressArr)
        return render(request, 'user/account/index.html',{"address":address})


def address_get_or_create(lineOne,city,country,post_code):
    address_obj = None
    address = Address.objects.filter(lineOne=lineOne,lineTwo=city,country=country,postCode=post_code)
    if address.exists():
        address_obj = address.first()
    else:
        address_obj = Address()
        address_obj.lineOne=lineOne
        address_obj.lineTwo=city
        address_obj.country=country
        address_obj.postCode=post_code
        address_obj.save()
    return address_obj



@login_required
def user_account_picture_view(request):
    if request.method == 'GET':
        return render(request, 'user/account/picture.html', context={})

    else:
        for f in request.FILES:
            request.user.person.profilePicture = request.FILES[f]
            request.user.person.save()

        return redirect('/user/account/picture')


@login_required
def user_account_views(request, view="index"):

    template = 'user/account/' + view + ".html"
    context = {}

    if view == "index":
        pass

    else:
        pass

    return render(request, template, context=context)


def user_account_confirmation_view(request):
    template = "confirmation.html"
    return render(request,template,context={})
@login_required
def change_password_view(request):
    if request.method == 'POST':
        message = None
        old_password = request.POST.get('old_password')
        new_password = request.POST.get('new_password')
        confirm_password = request.POST.get('confirm_password')
        if old_password != '' and new_password != '' and confirm_password!='':
            if request.user.check_password(old_password):
                if new_password!=confirm_password:
                    message = 'Password doesnt match'
                    messages.add_message(request, messages.WARNING,message)
                else:
                    request.user.set_password(new_password)
                    request.user.save()
                    message = 'Password Changed Successfully'
                    messages.add_message(request, messages.SUCCESS,message)
            else:
                message = 'Old Password doesnt match'
                messages.add_message(request, messages.WARNING,message)
        else:
            message = 'Password is empty'
            messages.add_message(request, messages.WARNING,message)

        update_session_auth_hash(request, request.user)
        return redirect('/user/account/security/')

@login_required
def user_account_license_view(request):
    if request.method == 'GET':
        license_documents = PersonDocument.objects.filter(person=request.user.person)
        return render(request, 'user/account/licence.html', context={'licences':license_documents.order_by('-id')[:2]})
    if request.method == 'POST':
        req_fields = ['fd','bd']
        req_inputs = ['ln','ls','c','ed']
        valid = True
        for f in req_fields:
            if f not in request.FILES or request.FILES[f] is None or request.FILES[f] == '':
                    valid = False

        for f in req_inputs:
            if f not in request.POST or request.POST[f] is None or request.POST[f] == '':
                    valid = False

        if not valid:
            messages.add_message(request, messages.WARNING,'Document/Input Missing')
        else:
            driver_license_front = request.FILES['fd']
            driver_license_back = request.FILES['bd']
            pdf=PersonDocument(person=request.user.person,document=driver_license_front,licence_number=request.POST['ln'],licence_state=request.POST['ls'],country=request.POST['c'],expiry=request.POST['ed'])
            pdf.save()
            pdb=PersonDocument(person=request.user.person,document=driver_license_back,licence_number=request.POST['ln'],licence_state=request.POST['ls'],country=request.POST['c'],expiry=request.POST['ed'])
            pdb.save()

            person = request.user.person
            person.approved = 0
            person.save()
            messages.add_message(request, messages.SUCCESS,'Document uploaded Successfully')
        return redirect('/user/account/licence/')

@login_required
def user_views(request, view="profile"):

    template = 'user/' + view + ".html"
    context = {}

    if view == "profile":
        context = {
            'messages': Message().user_messages(request.user.person),
            'booking_requests':Message().booking_request(request.user.person)
        }
        pass

    elif view == 'inbox':
        context = {
            'messages': Message().user_messages(request.user.person),
            'booking_requests':Message().booking_request(request.user.person)
        }

    elif view == 'history':
        context = {'history': Booking().bookings_for_user(request.user.person)}

    elif view == 'cars':
        context = {'cars': Vehicle.objects.filter(owner=request.user.person)}

    return render(request, template, context=context)


@login_required
def user_view_inbox_direct(request, with_user=None):
    if not with_user:
        # for a in request.GET:
        #     print(str(a) + ": " + str(request.GET[a]))
        #     print request.GET[""]
        # with_user = get_object_or_404(Person, id=)
        raise Http404

    print with_user
    print request.user.person.id
    if get_object_or_404(Person, id=with_user) == request.user.person:
        # TODO: Handle this better.
        raise Http404

    messages = Message.objects.filter(Q(sender=request.user.person) | Q(receiver=request.user.person)).filter(Q(sender=with_user) | Q(receiver=with_user)).filter(isBookingRequestMessage=False)
    return render(request, 'user/inbox_with.html', context={'messages': messages,'with_user':with_user})


@login_required
def message_reply(request):
    if request.method == 'POST':
        reply = request.POST.get('reply')
        receiver = request.POST.get('replied_to')
        sender = request.user.person.id
        message = Message()
        message.sender_id = int(sender)
        message.receiver_id = int(receiver)
        message.message = reply
        message.timestamp = timezone.now()
        message.save()
        response_data={}
        response_data['status']=True
        return HttpResponse(json.dumps(response_data), content_type="application/json")


@login_required
def user_booking_inbox(request,booking_ref_id):
    messages = Message.objects.filter(referencesBooking_id=booking_ref_id)
    return render(request, 'user/inbox_booking.html', context={'messages': messages})


def user_view_verify(request, a, b=None):
    code = ''
    if b:
        code = b

    else:
        code = a

    verify_info = Verification.objects.filter(code=code)

    # if request.user is not None and verify.person.user.id != request.user.id:
    #     raise Http404
    error = False
    for verify in verify_info:
        if verify.expires < pendulum.now('Australia/West'):
            error =True
        if verify.used:
            error =True

    user = None
    if not error:
        for verify in verify_info:
            user = verify.person.user
            verify.used = True
            verify.verified = True
            verify.verifiedTime = pendulum.now('Australia/West')
            verify.save()
    else:
        raise Http404

    if user is not None:
        login(request, user)

    return redirect('/user/')
