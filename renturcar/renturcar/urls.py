"""renturcar URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.8/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Add an import:  from blog import urls as blog_urls
    2. Add a URL to urlpatterns:  url(r'^blog/', include(blog_urls))
"""
from django.conf.urls import include, url
from django.conf.urls.static import static
from django.contrib import admin
from api.views import *
from django.conf import settings
urlpatterns = [
    # url(r'^admin/', include(admin.site.urls)),
    url(r'^admin/', include('adminapp.urls')),
    url(r'^login$', login_view),
    url(r'^logout$', logout_view),
    url(r'^signup/?$', signup_view),

    # User views.
    url(r'^user/?$', user_views),
    url(r'^user/(inbox)/?$', user_views),
    url(r'^user/inbox/with/(\w*)', user_view_inbox_direct),
    url(r'^user/message/reply', message_reply),
    url(r'^user/(cars)/?$', user_views),
    url(r'^user/(history)/?$', user_views),
    url(r'^user/verify/(.*)/(.*)?$', user_view_verify),
    url(r'^user/booking-inbox/(\w*)', user_booking_inbox),

    # User Account views.
    url(r'^user/account/?$', user_account_index_view),
    url(r'^user/account/picture/?$', user_account_picture_view),
    url(r'^user/account/licence/?$', user_account_license_view),
    url(r'^user/account/(payment)/?$', user_account_views),
    url(r'^user/account/(payout)/?$', user_account_views),
    url(r'^user/account/(security)/?$', user_account_views),
    url(r'^user/account/(cancel)/?$', user_account_views),
    url(r'^user/account/security/change-password/?$', change_password_view),
    url(r'^confirmation/?$', user_account_confirmation_view),
    url(r'^terms-and-conditions/?$', terms_and_conditions_view),




    url(r'^car/(\w*)/request/(\w*)?/?(\w*)?', car_request_view),
    url(r'^car/(\w*)/a/(.*)/(.*)', car_availability),
    url(r'^car/(\w*)/?.*', car_view, name='car_view'),

    url(r'^message', send_message_view),

    url(r'^search$', search_view, name='search_view'),

    url(r'^new$', new_listing_view, name='new_listing_view'),
    url(r'^requirements$', requirements_view),
    url(r'^contact/$', contact_view, name='contact_view'),


    url(r'^$', home_view),




]
if settings.DEBUG:
    #urlpatterns += static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
    urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
