#!/bin/bash
#
#
# One Command Deploy
#
#

# Jump into the venv.
DIR=$(dirname ${0})

VENV_PATH=$DIR"/renturcar/venv/"

if [ ! -e $VENV_PATH ]
then
    echo "Creating venv"
    virtualenv $VENV_PATH
fi

source $VENV_PATH/bin/activate

# Migrate the database.
python $DIR/renturcar/manage.py makemigrations
python $DIR/renturcar/manage.py migrate

# Ensure the correct version of ansible is installed.
pip uninstall -y ansible
pip install ansible==2.3.0.0

# Install everything else.
pip install -r $VENV_PATH/../requirements.txt

if [ $((`vagrant plugin list | grep vbguest | wc -l` + 0)) == 0 ]
then
    echo "Installing vagrant vbguest plugin"
    vagrant plugin install vagrant-vbguest
fi

#TODO: Update box.
vagrant up --provision

deactivate
